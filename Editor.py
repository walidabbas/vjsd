__author__ = "Walid Al-Mutwakel"
__version__ = "03.07.2021"
__maintainer__ = "Walid Al-Mutwakel"
__email__ = "walidalmutwakel@gmail.com"

import os
import comtypes.client
from docx import Document
import PySimpleGUIQt as sg
from Email import MailFiles

import pandas as pd

fileDir = os.path.dirname(os.path.realpath('__file__'))

rel_pathTempFile = os.path.join(fileDir, 'Temp')
rel_pathPDFsFile = os.path.join(fileDir, 'PDFs/')


def readExcel(rel_pathExcel):
    return pd.read_excel(rel_pathExcel)


def replace(rel_pathTemplate, rel_pathExcel, word_list):
    if not os.path.exists('Temp'):
        os.mkdir('Temp')

    df = readExcel(rel_pathExcel)
    for i in df.index:
        doc = Document(rel_pathTemplate)
        Dictionary = {}
        for words in word_list:
            first = words[0]
            second = df[words[1]][i]
            Dictionary[first] = second
        # Dictionary = {"FirstName": df["Vorname"][i], "LastName": df["Nachname"][i]}
        for j in Dictionary:
            for p in doc.paragraphs:
                if p.text.find(j) >= 0:
                    p.text = p.text.replace(j, Dictionary[j])
        sg.Print("Creating " + df["Vorname"][i] + ' ' + df["Nachname"][i] + '.docx in Folder Temp')
        doc.save('Temp/' + df["Vorname"][i] + ' ' + df["Nachname"][i] + '.docx')  # TODO
    sg.Print()
    sg.Print("Creating Word Files Done !!!")
    sg.Print("***********************************************")
    sg.Print()


def convertTpoPDF():
    if not os.path.exists('PDFs'):
        os.mkdir('PDFs')
    with os.scandir(rel_pathTempFile) as it:
        for entry in it:
            if entry.name.endswith(".docx") and entry.is_file():
                fileName = entry.name.replace(".docx", ".pdf")
                word = comtypes.client.CreateObject('Word.Application')
                doc = word.Documents.Open(entry.path)
                sg.Print("Creating " + fileName + " in " + rel_pathPDFsFile + fileName)
                doc.SaveAs(rel_pathPDFsFile + fileName, FileFormat=17)
                doc.Close()
                word.Quit()
    sg.Print()
    sg.Print("Creating Pdf Files Done !!!")
    sg.Print("***********************************************")
    sg.Print()
