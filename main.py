__author__ = "Walid Al-Mutwakel"
__version__ = "03.07.2021"
__maintainer__ = "Walid Al-Mutwakel"
__email__ = "walidalmutwakel@gmail.com"

import PySimpleGUIQt as sg
import sys
import Editor
from Email import MailFiles
import pandas as pd


def readInput(values, num_buttons):
    word_list = []
    if int(num_buttons) != 0:
        word_list = [values['IN'].split(',')]
        for i in range(int(num_buttons) - 1):
            word_list.append(values['IN' + str(i + 1)].split(','))
            sg.Print()
        sg.Print("The following words were selected to be replaced in the PDFs")
        sg.Print(word_list)
        sg.Print()
        sg.Print("Replacing Words in Word Template")
        sg.Print()
    Editor.replace(values['WORD'], values['EXCEL'], word_list)
    sg.Print("Convert the Word files to Pdf")
    sg.Print()
    Editor.convertTpoPDF()


def sendMails(values, user, password):
    from_addr = user
    pass_wd = password
    for i in values.index:
        to_addr = values["Email"][i]  # where the attachment should go to  #TODO
        memberName = 'PDFs\\' + values["Vorname"][i] + ' ' + values["Nachname"][i] + '.pdf'  # TODO
        sg.Print("sending email to: " + values["Email"][i])
        sg.Print()
        MailFiles(from_addr, pass_wd, to_addr, memberName)
        sg.Print()


class Example(object):
    # This is the normal print that comes with simple GUI
    sg.Print('Welcome to PDF sender  :)', do_not_reroute_stdout=False)
    sg.Print()

    sg.theme("DarkTeal2")
    layout = [[sg.Text('Please enter here how many words you want to replace:')],
              [sg.Input(do_not_clear=True, key='_NUMBER_')],
              [sg.T("")],
              [sg.Button('Submit')]]

    window = sg.Window('My File Browser', layout, size=(600, 100), icon='VJSD.ico')

    while True:
        event, values = window.Read()

        if event == sg.WIN_CLOSED or event == "Exit":
            sys.exit()
        elif event == "Create PDF":
            readInput(values, num_buttons)
            while True:
                event, values = window.Read()
                if event == "Send":
                    sendMails(pd.read_excel(values['EXCEL']), values['-USER-'], values['-PASSWORD-'])
                    sg.Print()
                    sg.Print("See the folder Done to see what files were sent ")
                    sg.Print()

                    while True:
                        event, values = window.Read()
                        if event == sg.WIN_CLOSED or event == "Exit":
                            sys.exit()
        elif event == "Send":
            sendMails(pd.read_excel(values['EXCEL']), values['-USER-'], values['-PASSWORD-'])
            while True:
                event, values = window.Read()
                if event == sg.WIN_CLOSED or event == "Exit":
                    sys.exit()

        num_buttons = values['_NUMBER_']
        sg.Print('you want to replace ' + num_buttons + ' Words')
        sg.Print()

        layout = [[sg.T("")], [sg.Text('Enter the file path', font='Default 18')],
                  [sg.T('Excel:', size=(8, 1)), sg.Input(key="EXCEL"), sg.FileBrowse()],
                  [sg.T('Word:', size=(8, 1)), sg.Input(key="WORD"), sg.FileBrowse()],
                  [sg.T("")],
                  [sg.Text(
                      'Enter the names from Word and from the column in Excel one after the other, separated by '
                      'commas, so that they are replaced \nE.g. like this: FirstName,Vorname')],
                  *[[sg.Input(do_not_clear=True, key='IN'), ] for i in range(int(num_buttons))],
                  [sg.T("")],
                  [sg.T('Mail login information', font='Default 18')],
                  [sg.T('Emil:', size=(8, 1)), sg.Input(key='-USER-')],
                  [sg.T('Password:', size=(8, 1)), sg.Input(password_char='*', key='-PASSWORD-')],
                  [sg.Button('Create PDF')], [sg.Button('Send')], [sg.Button('Exit')]]
        window1 = sg.Window('My File Browser', layout, size=(600, 150), icon='VJSD.ico', disable_close=True)
        window.Close()
        window = window1

    def run(self):
        pass


if __name__ == '__main__':
    Example().run()
