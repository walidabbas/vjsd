"""
author: Pure Python
url: https://www.purepython.org
copyright: CC BY-NC 4.0
creation date: 02-01-2019

Small script to send mails with attachment from folder with Gmail

Uses nothing but python 3 modules.

Create a folder named mail_files and within this folder done and new.
You can place all the attachments within the new folder.
When the script is ready all attachments send will be in the Done folder.
If the script crashes, you can rerun the script as it will start
where it stopped.

You need an gmail app password for this to work
https://support.google.com/accounts/answer/185833?hl=en
"""

__editor__ = "Walid Al-Mutwakel"
__version__ = "03.07.2021"
__maintainer__ = "Walid Al-Mutwakel"
__email__ = "walidalmutwakel@gmail.com"

import os
import time
import smtplib

from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders


class MailFiles:
    def __init__(self, from_addr=None, pass_wd=None, to_addr=None, memberName=None):
        """
        init function for this to work
        :param from_addr: your gmail account email address
        :param pass_wd: your gmail app pwd (see discription above)
        :param to_addr: here the attachment should go to
        """
        self.from_addr = from_addr
        self.pass_wd = pass_wd
        self.to_addr = to_addr

        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.start_dir = os.path.join(dir_path, "PDFs")
        self.done_dir = os.path.join(dir_path, "Done")
        self.memberName = os.path.join(dir_path, memberName)

        self.process_files()

    def process_files(self):
        """
        This will loop thru the files in the New folder and calls the
        mail_file function. It has a sleep function so Gmail will not
        be flooded.
        """
        i = 1
        for filename in os.listdir(self.start_dir):
            file = os.path.join(self.start_dir, filename)
            if self.memberName == file:
                if self.mail_file(file, filename):  # Mailing returns True or False
                    backup_file = os.path.join(self.done_dir, filename)
                    if not os.path.exists(file):
                        os.rename(file, backup_file)  # move done file to done folder
                    print("Document {} send!".format(filename))
                i += 1
                time.sleep(5)  # Sleep for 5 seconds to not overrun Gmail

    def mail_file(self, file=None, filename=None):
        """
        This function will mail the file to the email address as an attachment
        :param file: path with filename
        :param filename: filename online
        :return: true or false
        """
        try:
            msg = MIMEMultipart()

            msg['From'] = self.from_addr
            msg['To'] = self.to_addr
            msg['Subject'] = "Sending file {}".format(filename)

            body = "Sending File {} to you\n\n\n\nLiebe Grüße\nVJSD\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" \
                   "Edit by W@lid".format(
                filename)
            msg.attach(MIMEText(body, 'plain'))

            attachment = open(file, "rb")

            part = MIMEBase('application', 'octet-stream')
            part.set_payload(attachment.read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

            msg.attach(part)

            serverTyp = self.getServer()
            server = smtplib.SMTP(serverTyp, 587)
            server.starttls()
            server.login(self.from_addr, self.pass_wd)
            text = msg.as_string()
            server.sendmail(self.from_addr, self.to_addr, text)
            server.quit()

            return True
        except smtplib.SMTPException as e:
            print(e)
            return False
        except:
            return False

    def getServer(self):
        servers = {
            '1&1': 'Smtp.1and1.com',
            'Airmail': 'Mail.airmail.net',
            'AOL': 'Smtp.aol.com',
            'AT&T': 'Outbound.att.net',
            'Bluewin': 'Smtpauths.bluewin.ch ',
            'BT Connect': 'Mail.btconnect.tom',
            'Comcast': 'Smtp.comcast.net',
            'Earthlink': 'Smtpauth.earthlink.net',
            'Gmail': 'Smtp.gmail.com',
            'Gmx': 'Mail.gmx.net',
            'HotPop': 'Mail.hotpop.com',
            'Libero': 'Mail.libero.it',
            'Lycos': 'Smtp.lycos.com',
            'O2': 'Smtp.o2.com',
            'Orange': 'Smtp.orange.net',
            'Hotmail': 'smtp.office365.com',
            'T-Online': 'Securesmtp.t-online.de',
            'Tiscali': 'Smtp.tiscali.co.uk',
            'Verizon': 'Outgoing.verizon.net',
            'Virgin': 'Smtp.virgin.net',
            'Web.de': 'Smtp.web.de',
            'Yahoo': 'Smtp.mail.yahoo.com',
            'vjsd': 'smtp.ionos.de',
        }
        email = self.from_addr
        for key, value in servers.items():
            if email.lower().find(key.lower()) != -1:
                return servers.get(key)
